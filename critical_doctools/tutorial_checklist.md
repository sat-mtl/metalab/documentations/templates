# Tutorials: a quality checklist

What do we look for in a tutorial?

Questions to ask yourself, with no specific order:

* Does the tutorial compiles ? By this, we mean that all the steps in the tutorial work as intended, that lines of codes have no typos or other mistakes that might prevent someone from successfully complete the tutorial.
* While testing my tutorial, was I able to complete all the steps? Was I able to complete them without needing outside assistance ?
* In other words, is all the information needed for the tutorial in the tutorial?
* If any information is outside the scope of the tutorial, is it possible to refer the user to other sources of information? This could be through adding links to a relevant FAQ or troubleshooting section.

Now that the tutorial is in a working state, we can think about making it better. Here are some suggestions:

* Does everything in the tutorial belongs there - or should some of it be part of another section of the docs? [According to the documentation system presented here](https://documentation.divio.com/), there are four different types of documentation: tutorials, how-to guides, explanation, reference. A tutorial should be about giving step-by-step instructions aimed at teaching a new user how to do something with the software. The goal is to learn. If the tutorial is trying to solve problems, these sections should belong in a how-to guide. If there are long, detailed explanations, they should be in the description section of this site. And last, if the tutorial contains lots of information about the API, it should be in the API reference documentation.
* Is the goal of the tutorial clear and well-explained in the first sentence or two?
* Is the level of difficulty of the tutorial clear?
* Is there a list of all things needed for the tutorial before the tutorial starts?
* Do all steps have a well-defined title that helps in keeping track of where we are in the tutorial?
* Are all steps numbered?
* Is there any step hidden in plain sight, in the middle of a paragraph or in an image, e.g.?
* Do all images have alt text to describe what is in the image?
* Same for videos and other media included in the tutorial.
* If we need any outside media for the tutorial, do we link to possible source of such media for the tutorial purposes? E.g. linking to a possible Creative Commons bank of images to be used for the project.
* Are there links to other interesting tutorials that could follow the current one?
* Is there use of **bold**, *italics* or `code` styles? Is the use accurate and thoughtful, or is it distracting?
* Are code sections correctly understood by the static site generator - with appropriate highlighting?
* Do we help the user identifying if steps have been correctly followed, by given potential tests to do or cues to look out for?
* Is there a clear end to the tutorial?
* Are there multiple subtopics in the tutorial that should have their own tutorial?
* Is there a time estimate for succesful completion of the tutorial?
* Are there any typo, grammar or vocabulary mistakes?



