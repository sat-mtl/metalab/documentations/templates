*The Code of Conduct is about setting expectations and standards regarding the community around the $PROJECT.*

*It should include who to contact if something is wrong according to the CoC.*

*It should explain what is the process around a CoC potential violation.*

*We could duplicate the CoC on our website and on the repo.*

--------------------------------------------------------------------------------

# Code of Conduct

Here is the Code of Conduct, including explanations about the importance of having a CoC.
