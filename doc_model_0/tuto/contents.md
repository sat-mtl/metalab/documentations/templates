*Tutorials should be about accomplishing **small projects**.*

*The goal of a tutorial is to help the user get started using the tool.*

*A tutorial should be self-contained and specific.*

-------------------------------------------------------------------------------

# Tutorials

Here are projects ideas to help you get started using the $PROJECT.

## Your first project with $PROJECT

A demo project that should show the main usecase for the $PROJECT.

The demo should include all materials needed for its completion, meaning if we need to use images, 3D models, sound files or other external content, we should provide demo materials.

## Second projects

Now that the user is onboarded and is more familiar with the $PROJECT, we can address more specific usecase.


