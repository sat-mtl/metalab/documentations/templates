*this is meant to become index.html after the docs are generated. it is meant to be the landing page.*

-------------------------------------------------------------

# $PROJECT_NAME 's documentation

This is the $PROJECT documentation website.

## description of the project

General overview. [link to other metalab projects]. Specific, more technical details.

## Demo of the project

include video, image, gif or other media that shows what is the end result of the project usecases.

## Table of Contents

* [link to install]
* [link to tutorial one] is meant to be your first demo project of the tool.
* [link to all tutorials] includes all of our guides to common projects.
* [link to howto] is used to give you step-by-step instruction to specific tasks
* [link to faq] for all your troubleshooting needs

## Want to get in touch ?

[link to community ]- [link to contact]


## Want to contribute to the project ?

[link to code of conduct] - [ link to contributing ]

## show me the code

[link to gitlab repo]

## sponsors

thanks to $SPONSORS for their support :)

