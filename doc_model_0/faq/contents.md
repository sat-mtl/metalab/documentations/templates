*the FAQ folder contains Frequently Asked Questions and troubleshooting common issues.*

*These questions might link to other parts of the website if needed be - we should not double the information.*

*We could start the FAQ with questions we had about the project when we first started working on it.*

*We should add questions asked by contributors as they started working on the project.*

*FAQs do not need to be specific, but they should be about the project. If the best answer to the question is to be found elsewhere, we could consider linking to that site...*

*It might be better to have a single page with all the questions first - and then, if the page grows too long, we could split each category into its own folder.*

-------------------------------------------------------------------------------

# Frequently Asked Questions - Foire Aux Questions

If your question is not on the list, please [do this ( add an issue to the gitlab repo, ask your question in the mailing list / on this platform, other )].

## $CATEGORY_QUESTION_X

### I am not able to do X.

### X does not work.

{for each cateogry in category_questions}

## VARIA

Include all questions who do not have a clear category for them.


