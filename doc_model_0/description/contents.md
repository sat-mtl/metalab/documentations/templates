*The explanations folder contains information about the project that aims at making explicit certain choices that were made along the way about the project's design, features, development strategies and similar.*

*The goal is to answer the 'why' of the choices we made about how to do things.*

*The goal is to help the user understand our project, not show how to use it.*
--------------------------------------------------------------------------------

# Explanations

Why we build things that way; why we made these choices.

Could include links to past talks, presentations, research papers...

Here are some examples of possible questions to answer:

## Why we chose to build with this programming language instead of another

## How we made our design decisions

## How does this tool fit with our other tools

## How does this tool fit in with tools made elsewhere

## How we implemented this general standard



