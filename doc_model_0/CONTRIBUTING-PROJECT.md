*Contributing is about the necessary steps to build the -dev version of the documented project.*

--------------------------------------------------------------------------------

# Contributing

Our documentation project follows [Code of Conduct link]. It applies to all contributions.

## Installing the -dev branch of the project

## Building the project locally

## Making a change to the project

## Testing the change locally

## Pushing the branch to Gitlab

## Making a MR

## Following up on a MR

## What to expect next

## Adding your name to AUTHORS.md

This step might have been automated, depending on the project.


