*This README is the README on the gitlab repo. it is not meant to be deployed to a website.*
*the goal of the README is to contain the information about a project one would need to know if they were to discover the project through gitlab, or through a site that links to the gitlab repo.*


--------------------------------------------------------------------------------------------

# The README of the project

[insert .gif demo of the project, or some image at least]

## License

License of the project

## Links

[link to the main website] - [link to the documentation website] - [link to get in touch webpage] - [link to API reference]

## Table of contents

[[_TOC_]]

## What is this project

High-level description of the project

## Technical considerations

Technical description of the project

[link to explanations in the doc] for more technical information.

[link to API doc] for more technical specification.

## Get started

### Installation

How to install the project.

### First few steps

Ways to test that the project installation worked correctly.

### Next steps

#### Tutorials

Link to tutorials, with maybe a suggestion of a starting point.

#### How-tos

Look up specific tasks here: [link to the how-tos documentation]

## Code of Conduct

Description of the main points in plain natural language.

[link to the complete CoC]

## Contributing

Description of the main points of how to contribute & mention of the CoC.

How to find good issues to work on. Community info: see get in touch.

[link to CONTRIBUTING.md]

## Get in touch

Why they should get in touch in us :)

How to get in touch: [links to platforms], [mailinglist]

## Sponsors

Info and links to sponsors.

## About us

Who we are, why are doing this, how it started...

[link to authors.md // contributors.md ]





