*This guide is about giving specific instructions on how to install the project on the user's computer.*

--------------------------------------------------------------------------------

# Installation

To install the project, do these steps according to your OS.

Also includes a link to [INSTALLATION.md] for the -dev version of the project, for people who wish to contribute.
