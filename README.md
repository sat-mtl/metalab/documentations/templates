# Documentation: a model

[[_TOC_]]

This repo is meant to represent two things:
* how we could structure the documentation for projects in general
* what kind of content we want to include *by default* in each project doc.

It follows [the 4 types of documentation](https://documentation.divio.com/), with the added sections of `install` and `faq`. 

It is meant to be a starting point, not a definitive answer.

Each folder contains one possible structure for the documentation of a project.

## Folder `doc_model_0`

This is the first proposition for a documentation model. It is meant to be a draft we can work on improving.

Files whose name is in CAPS are meant to be accessible from gitlab. Files whose names are in lower case are meant to be rendered by a static site generator.

Each file contain a meta-data at the top of the file that explains what the file is about.

### Contributing to `doc_model_0`

#### To make small modifications to the template

This is a collaborative template : contributions are welcome.

To contribute to this project, make a local copy of the project with `git clone`.

Create a new branch and switch to that branch: `git checkout -b NAMEOFBRANCH`. `NAMEOFBRANCH` should be either `feature/change-something`, `feature/add-something`, or `bug/fix-something`.

Make your changes in the branch `NAMEOFBRANCH`.

When you are happy with these changes, make a Merge Request (MR):
* first, push your local changes to gitlab by executing `git push origin NAMEOFBRANCH`
* second, open a MR explaining your changes.

#### To change the template in a meaningful way

Please consider starting a new template by opening a folder with a meaningful name, or simply increasing the folder count.

I would prefer to have all templates available in the root folder as folders themselves than as branches - the goal is not to have An Optimal Template, the goal is closer to have a selection of valid templates to pick from for a specific project.


## Folder `critical_doctools`

The folder `critical_doctools` contains meta-information about documentation. For now, it is meant to be a toolbox, holding various useful tools. 

### Tutorial analysis: a checklist

The goal of this checklist is to help the documentation contributor in evaluating the quality of a given tutorial.

